import Login from "src/models/Login";
import notify, {TypeMessage} from "src/mixins/notification";

import jwt from "jsonwebtoken";
import DefaultService from "src/services/DefaultService";

export default class AuthApiService extends DefaultService<Login> {

  public async generateJwt(loginForm: Login) {
    try {
      const tokenData = await this.client.post('auth', loginForm);
      return tokenData.token;
    } catch (e) {
      notify('Não foi possível autenticar na API', TypeMessage.error);
    }
  }

  public verifyToken(token: string | null) {
    if (token === null) return false;

    try {
      jwt.verify(token, Buffer.from('VGFrZSBPbiBNZSBUYWtlIE1lIE9u', 'base64'), {
        subject: 'Hybrid Recommendation System - SRH'
      })
      return true;
    } catch (e) {
      return false;
    }
  };
}
