import Login from "src/models/Login";
import notify, {TypeMessage} from "src/mixins/notification";

// @ts-ignore
import {Store} from "src/store";
import DefaultService from "src/services/DefaultService";

export default class LoginService extends DefaultService<Login>{

  async login(username: string, password: string) {
    const loginForm = new Login(username, password);

    try {
      const resp = await this.client.post('/admins/login', loginForm);

      if (resp.validUser) {
        // @ts-ignore
        Store.commit('login/defineUserId', resp.userId);
        notify('Login realizado com sucesso!', TypeMessage.success);
        return true;
      }
    } catch (e) {
      notify('Não foi possível realizar o login', TypeMessage.error);
      return false;
    }
  }
}
