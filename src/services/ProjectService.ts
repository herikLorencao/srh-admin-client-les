import DefaultService from "src/services/DefaultService";
import Project from "src/models/Project";
import notify, {TypeMessage} from "src/mixins/notification";

export default class ProjectService extends DefaultService<Project>{
  async findAll() {
    try {
      const resp = await this.client.list('/projects')
      return resp._embedded.projects;
    } catch (e) {
      notify('Não foi possível buscar os projetos', TypeMessage.error);
      return false;
    }
  }

  async create(project: Project) {
    try {
      const resp = await this.client.post('/projects', project);
      notify('Projeto criado com sucesso', TypeMessage.success);
      return resp.data;
    } catch (e) {
      this.handleErrors(e.response, project);
      notify('Não foi possível criar o projeto', TypeMessage.error);
      return false;
    }
  }

  async update(project: Project, id: number) {
    try {
      const resp = await this.client.put('/projects', id, project);
      return resp.data;
    } catch (e) {
      this.handleErrors(e.response, project);
      notify('Não foi possível alterar o projeto', TypeMessage.error);
      return false;
    }
  }
}
