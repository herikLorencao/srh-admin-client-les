import axios, {AxiosInstance} from 'axios'
import FormRequest from "src/models/FormRequest";
// @ts-ignore
import {Store} from "src/store";

export default class Client<T extends FormRequest> {
  private client: AxiosInstance;

  constructor() {
    // @ts-ignore
    const token = Store.getters['login/getToken'];

    this.client = axios.create({
      baseURL: 'http://localhost:8080',
      headers: {
        Authorization: 'Bearer ' + token
      }
    });
  }

  public async get(uri: string, id: number) {
    const resp = await this.client.get(`${uri}/${id}`);
    return resp.data;
  }

  public async list(uri: string) {
    const resp = await this.client.get(uri);
    return resp.data;
  }

  public async post(uri: string, data: T) {
    const resp = await this.client.post(uri, data.build());
    return resp.data;
  }

  public async put(uri: string, id: number, data: T) {
    const resp = await this.client.put(`${uri}/${id}`, data.build())
    return resp.data;
  }
}
