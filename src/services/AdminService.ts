import Admin from "src/models/Admin";
import notify, {TypeMessage} from "src/mixins/notification";
import DefaultService from "src/services/DefaultService";

export default class AdminService extends DefaultService<Admin>{

  public async find(id: number) {
    try {
      const resp = await this.client.get('/users/admins', id);
      return new Admin(
        id, resp.name,
        resp.login, resp.email
      );
    } catch (e) {
      notify('Não foi possível encontrar seus dados da conta', TypeMessage.error);
    }
  }
}
