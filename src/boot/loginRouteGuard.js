import AuthApiService from "src/services/AuthApiService";
import Login from '../models/Login';

export default async({ router, store }) => {
  router.beforeEach((to, from, next) => {
    const token = store.getters['login/getToken'];
    const authApiService = new AuthApiService();

    if (authApiService.verifyToken(token)) next();

    const loginForm = new Login('admin', '123456');
    authApiService.generateJwt(loginForm).then(resp => {
      store.commit('login/defineToken', resp);
    });

    next();
  })
}
