interface Login {
  userId: Number | null,
  token: String | null
}

export interface LoginStateInterface {
  userLoginInfo: Login;
}

const state: LoginStateInterface = {
  userLoginInfo: {
    userId: null,
    token: null
  },
};

export default state;
