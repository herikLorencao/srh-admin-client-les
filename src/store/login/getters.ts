import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { LoginStateInterface } from './state';

const getters: GetterTree<LoginStateInterface, StateInterface> = {
  infoUsuario(state: any) {
    return state.userLoginInfo;
  },
  getIdUsuario(state: any) {
    return state.userLoginInfo.userId;
  },
  getToken(state: any) {
    return state.userLoginInfo.token;
  }
};

export default getters;
