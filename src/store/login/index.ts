import {Module} from 'vuex';
import {StateInterface} from '../index';
import state, {LoginStateInterface} from './state';
import getters from "src/store/login/getters";
import mutations from "src/store/login/mutations";

const loginModule: Module<LoginStateInterface, StateInterface> = {
  namespaced: true,
  getters,
  state,
  mutations
}

export default loginModule;
