import { MutationTree } from 'vuex';
import {LoginStateInterface} from './state';

const mutation: MutationTree<LoginStateInterface> = {
  defineToken(state: any, token) {
    state.userLoginInfo.token = token;
  },
  defineUserId(state: any, id: number | null) {
    state.userLoginInfo.userId = id;
  }
};

export default mutation;
