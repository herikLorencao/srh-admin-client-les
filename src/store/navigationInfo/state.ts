interface NavigationInfo {
  projectId: number | null
}

export interface NavigationInfoStateInterface {
  navigationInfo: NavigationInfo;
}

const state: NavigationInfoStateInterface = {
  navigationInfo: {
    projectId: null
  }
};

export default state;
