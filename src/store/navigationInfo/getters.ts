import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import {NavigationInfoStateInterface} from "src/store/navigationInfo/state";

const getters: GetterTree<NavigationInfoStateInterface, StateInterface> = {
  projectId(state: any) {
    return state.navigationInfo.projectId;
  },
};

export default getters;
