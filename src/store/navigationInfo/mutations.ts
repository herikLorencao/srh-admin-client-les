import { MutationTree } from 'vuex';
import {NavigationInfoStateInterface} from 'src/store/navigationInfo/state';

const mutation: MutationTree<NavigationInfoStateInterface> = {
  defineProjectId(state: any, id: number | null) {
    state.navigationInfo.projectId = id;
  }
};

export default mutation;
