import {Module} from 'vuex';
import {StateInterface} from '../index';
import state, {NavigationInfoStateInterface} from 'src/store/navigationInfo/state';
import getters from "src/store/navigationInfo/getters";
import mutations from "src/store/navigationInfo/mutations";

const loginModule: Module<NavigationInfoStateInterface, StateInterface> = {
  namespaced: true,
  getters,
  state,
  mutations
}

export default loginModule;
