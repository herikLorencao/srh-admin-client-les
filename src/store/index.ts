import {store} from 'quasar/wrappers';
import Vuex from 'vuex';

// @ts-ignore
import login from './login/index';
import navigationInfo from './navigationInfo/index';
// @ts-ignore
import {LoginStateInterface} from "src/store/login/state";
import {NavigationInfoStateInterface} from "src/store/navigationInfo/state";

export interface StateInterface {
  login: LoginStateInterface;
  navigationInfo: NavigationInfoStateInterface;
}

let Store = null;

export default store(({ Vue }) => {
  Vue.use(Vuex);

  Store = new Vuex.Store<StateInterface>({
    modules: {
      login,
      navigationInfo
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEV,
  });

  return Store;
});

export {Store}
