import {Notify} from 'quasar'

export const TypeMessage = {
  success: 'positive',
  warning: 'warning',
  info: 'info',
  error: 'negative'
}

export default function notify(this: any, message: string, type: string) {
  Notify.create({message, type, position: 'bottom-right'});
}
