import {ItemMenu} from "src/models/models";

const values: ItemMenu[] = [
  {
    id: 1,
    path: '/projetos',
    label: 'Projetos',
    icon: 'attach_file',
    subitems: [
      {
        id: 1,
        label: 'Usuários',
        path: 'projetos/usuarios',
        icon: 'person'
      },
      {
        id: 2,
        label: 'Recomendações',
        path: 'projetos/recomendacoes',
        icon: 'emoji_objects'
      },
      {
        id: 3,
        label: 'Avaliações',
        path: 'projetos/avaliacoes',
        icon: 'edit'
      },
      {
        id: 4,
        label: 'Tags',
        path: 'projetos/tags',
        icon: 'local_offer'
      },
      {
        id: 5,
        label: 'Tipos de Item',
        path: 'projetos/tipositens',
        icon: 'category'
      },
      {
        id: 6,
        label: 'Itens',
        path: 'projetos/itens',
        icon: 'pageview'
      }
    ]
  },
  {
    id: 2,
    path: 'tags',
    label: 'Tags',
    icon: 'local_offer',
    subitems: []
  },
  {
    id: 3,
    path: 'tipoitens',
    label: 'Tipos de Item',
    icon: 'category',
    subitems: []
  },
  {
    id: 4,
    path: "apis",
    label: 'APIs',
    icon: 'settings',
    subitems: []
  },
];

export default values;
