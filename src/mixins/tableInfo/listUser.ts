export default {
  columns: [
    {
      name: 'id',
      field: 'id',
      label: 'Código',
      align: 'center',
      sortable: true
    },
    {
      name: 'name',
      field: 'name',
      label: 'Nome',
      align: 'center',
      sortable: true
    },
    {
      name: 'email',
      field: 'email',
      label: 'Email',
      align: 'center',
      sortable: true
    }
  ]
}
