export default {
  pagination: {
    sortBy: 'desc',
    descending: false,
    page: 1,
    rowsPerPage: 3
  },
  columns: [
    {
      name: 'name',
      field: 'name',
      label: 'Nome',
      align: 'center',
      sortable: true
    }
  ]
}
