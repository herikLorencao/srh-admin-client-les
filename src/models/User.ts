import FormRequest from "src/models/FormRequest";
import {ParseItem} from "src/models/models";

export default class User extends FormRequest {
  private _name: string;
  private _login: string;
  private _password: string;
  private _email: string;

  public parseNames: ParseItem[] = [
    {
      fieldName: 'name',
      translation: 'nome'
    },
    {
      fieldName: 'description',
      translation: 'descrição'
    }
  ];


  constructor(name: string, login: string, password: string, email: string, parseNames: ParseItem[]) {
    super();
    this._name = name;
    this._login = login;
    this._password = password;
    this._email = email;
    this.parseNames = parseNames;
  }


  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  public build(): object {
    return {
      name: this._name,
      login: this._login,
      password: this._password,
      email: this._email
    }
  }
}
