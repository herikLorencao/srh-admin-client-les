import {ParseItem} from "src/models/models";

export default abstract class FormRequest {
  build(): object {
    return {}
  }

  public parseNames: ParseItem[] = [];
}
