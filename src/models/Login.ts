import FormRequest from "src/models/FormRequest";

export default class Login extends FormRequest{
  private _login: string;
  private _password: string;


  constructor(login: string, password: string) {
    super();
    this._login = login;
    this._password = password;
  }


  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  public build(): object {
    return {
      login: this._login,
      password: this._password
    }
  }
}
