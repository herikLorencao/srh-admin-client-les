import FormRequest from "src/models/FormRequest";
import {ParseItem} from "src/models/models";

export default class Project extends FormRequest {
  private _name: string;
  private _description: string;
  private _adminId: number;
  private _situation: string;
  private _visible: boolean;

  public parseNames: ParseItem[] = [
    {
      fieldName: 'name',
      translation: 'nome'
    },
    {
      fieldName: 'description',
      translation: 'descrição'
    }
  ];

  constructor(name: string, description: string, adminId: number, situation: string, visible: boolean) {
    super();
    this._name = name;
    this._description = description;
    this._adminId = adminId;
    this._situation = situation;
    this._visible = visible;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get adminId(): number {
    return this._adminId;
  }

  set adminId(value: number) {
    this._adminId = value;
  }

  get situation(): string {
    return this._situation;
  }

  set situation(value: string) {
    this._situation = value;
  }

  get visible(): boolean {
    return this._visible;
  }

  set visible(value: boolean) {
    this._visible = value;
  }

  public build(): object {
    return {
      name: this._name,
      description: this._description,
      adminId: this._adminId,
      situation: this._situation,
      visible: this._visible
    };
  }
}
