export interface SubItem {
  id: number,
  label: string,
  path: string,
  icon: string
}

export interface ItemMenu {
  id: number,
  label: string,
  path: string,
  icon: string,
  subitems: SubItem[]
}


export interface ParseItem {
  fieldName: string,
  translation: string
}
