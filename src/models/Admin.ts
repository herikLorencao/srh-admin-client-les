import FormRequest from "src/models/FormRequest";

export default class Admin extends FormRequest {
  private _id: number;
  private _name: string;
  private _login: string;
  private _email: string;

  constructor(id: number, name: string, login: string, email: string) {
    super();
    this._id = id;
    this._name = name;
    this._login = login;
    this._email = email;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  public build(): object {
    return {
      id: this._id,
      name: this._name,
      login: this._login,
      email: this._email
    }
  }
}
